define('fs/test/assert', [], function(require, exports, module) {
��/ /   h t t p : / / w i k i . c o m m o n j s . o r g / w i k i / U n i t _ T e s t i n g / 1 . 0  
 / /  
 / /   T H I S   I S   N O T   T E S T E D   N O R   L I K E L Y   T O   W O R K   O U T S I D E   V 8 !  
 / /  
 / /   C o p y r i g h t   ( c )   2 0 1 1   J x c k  
 / /  
 / /   O r i g i n a l l y   f r o m   n o d e . j s   ( h t t p : / / n o d e j s . o r g )  
 / /   C o p y r i g h t   J o y e n t ,   I n c .  
 / /  
 / /   P e r m i s s i o n   i s   h e r e b y   g r a n t e d ,   f r e e   o f   c h a r g e ,   t o   a n y   p e r s o n   o b t a i n i n g   a   c o p y  
 / /   o f   t h i s   s o f t w a r e   a n d   a s s o c i a t e d   d o c u m e n t a t i o n   f i l e s   ( t h e   ' S o f t w a r e ' ) ,   t o  
 / /   d e a l   i n   t h e   S o f t w a r e   w i t h o u t   r e s t r i c t i o n ,   i n c l u d i n g   w i t h o u t   l i m i t a t i o n   t h e  
 / /   r i g h t s   t o   u s e ,   c o p y ,   m o d i f y ,   m e r g e ,   p u b l i s h ,   d i s t r i b u t e ,   s u b l i c e n s e ,   a n d / o r  
 / /   s e l l   c o p i e s   o f   t h e   S o f t w a r e ,   a n d   t o   p e r m i t   p e r s o n s   t o   w h o m   t h e   S o f t w a r e   i s  
 / /   f u r n i s h e d   t o   d o   s o ,   s u b j e c t   t o   t h e   f o l l o w i n g   c o n d i t i o n s :  
 / /  
 / /   T h e   a b o v e   c o p y r i g h t   n o t i c e   a n d   t h i s   p e r m i s s i o n   n o t i c e   s h a l l   b e   i n c l u d e d   i n  
 / /   a l l   c o p i e s   o r   s u b s t a n t i a l   p o r t i o n s   o f   t h e   S o f t w a r e .  
 / /  
 / /   T H E   S O F T W A R E   I S   P R O V I D E D   ' A S   I S ' ,   W I T H O U T   W A R R A N T Y   O F   A N Y   K I N D ,   E X P R E S S   O R  
 / /   I M P L I E D ,   I N C L U D I N G   B U T   N O T   L I M I T E D   T O   T H E   W A R R A N T I E S   O F   M E R C H A N T A B I L I T Y ,  
 / /   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E   A N D   N O N I N F R I N G E M E N T .   I N   N O   E V E N T   S H A L L   T H E  
 / /   A U T H O R S   B E   L I A B L E   F O R   A N Y   C L A I M ,   D A M A G E S   O R   O T H E R   L I A B I L I T Y ,   W H E T H E R   I N   A N  
 / /   A C T I O N   O F   C O N T R A C T ,   T O R T   O R   O T H E R W I S E ,   A R I S I N G   F R O M ,   O U T   O F   O R   I N   C O N N E C T I O N  
 / /   W I T H   T H E   S O F T W A R E   O R   T H E   U S E   O R   O T H E R   D E A L I N G S   I N   T H E   S O F T W A R E .  
  
 ( f u n c t i o n ( g l o b a l )   {  
  
 / /   O b j e c t . c r e a t e   c o m p a t i b l e   i n   I E  
 v a r   c r e a t e   =   O b j e c t . c r e a t e   | |   f u n c t i o n ( p )   {  
     i f   ( ! p )   t h r o w   E r r o r ( ' n o   t y p e ' ) ;  
     f u n c t i o n   f ( )   { } ;  
     f . p r o t o t y p e   =   p ;  
     r e t u r n   n e w   f ( ) ;  
 } ;  
  
 / /   U T I L I T Y  
 v a r   u t i l   =   {  
     i n h e r i t s :   f u n c t i o n ( c t o r ,   s u p e r C t o r )   {  
         c t o r . s u p e r _   =   s u p e r C t o r ;  
         c t o r . p r o t o t y p e   =   c r e a t e ( s u p e r C t o r . p r o t o t y p e ,   {  
             c o n s t r u c t o r :   {  
                 v a l u e :   c t o r ,  
                 e n u m e r a b l e :   f a l s e ,  
                 w r i t a b l e :   t r u e ,  
                 c o n f i g u r a b l e :   t r u e  
             }  
         } ) ;  
     }  
 } ;  
  
 v a r   p S l i c e   =   A r r a y . p r o t o t y p e . s l i c e ;  
  
 / /   1 .   T h e   a s s e r t   m o d u l e   p r o v i d e s   f u n c t i o n s   t h a t   t h r o w  
 / /   A s s e r t i o n E r r o r ' s   w h e n   p a r t i c u l a r   c o n d i t i o n s   a r e   n o t   m e t .   T h e  
 / /   a s s e r t   m o d u l e   m u s t   c o n f o r m   t o   t h e   f o l l o w i n g   i n t e r f a c e .  
  
 v a r   a s s e r t   =   o k ;  
  
 g l o b a l [ ' a s s e r t ' ]   =   a s s e r t ;  
  
 i f   ( t y p e o f   m o d u l e   = = =   ' o b j e c t '   & &   t y p e o f   m o d u l e . e x p o r t s   = = =   ' o b j e c t ' )   {  
     m o d u l e . e x p o r t s   =   a s s e r t ;  
 } ;  
  
 / /   2 .   T h e   A s s e r t i o n E r r o r   i s   d e f i n e d   i n   a s s e r t .  
 / /   n e w   a s s e r t . A s s e r t i o n E r r o r ( {   m e s s a g e :   m e s s a g e ,  
 / /                                                           a c t u a l :   a c t u a l ,  
 / /                                                           e x p e c t e d :   e x p e c t e d   } )  
  
 a s s e r t . A s s e r t i o n E r r o r   =   f u n c t i o n   A s s e r t i o n E r r o r ( o p t i o n s )   {  
     t h i s . n a m e   =   ' A s s e r t i o n E r r o r ' ;  
     t h i s . m e s s a g e   =   o p t i o n s . m e s s a g e ;  
     t h i s . a c t u a l   =   o p t i o n s . a c t u a l ;  
     t h i s . e x p e c t e d   =   o p t i o n s . e x p e c t e d ;  
     t h i s . o p e r a t o r   =   o p t i o n s . o p e r a t o r ;  
     v a r   s t a c k S t a r t F u n c t i o n   =   o p t i o n s . s t a c k S t a r t F u n c t i o n   | |   f a i l ;  
  
     i f   ( E r r o r . c a p t u r e S t a c k T r a c e )   {  
         E r r o r . c a p t u r e S t a c k T r a c e ( t h i s ,   s t a c k S t a r t F u n c t i o n ) ;  
     }  
 } ;  
  
 / /   a s s e r t . A s s e r t i o n E r r o r   i n s t a n c e o f   E r r o r  
 u t i l . i n h e r i t s ( a s s e r t . A s s e r t i o n E r r o r ,   E r r o r ) ;  
  
 f u n c t i o n   r e p l a c e r ( k e y ,   v a l u e )   {  
     i f   ( v a l u e   = = =   u n d e f i n e d )   {  
         r e t u r n   ' '   +   v a l u e ;  
     }  
     i f   ( t y p e o f   v a l u e   = = =   ' n u m b e r '   & &   ( i s N a N ( v a l u e )   | |   ! i s F i n i t e ( v a l u e ) ) )   {  
         r e t u r n   v a l u e . t o S t r i n g ( ) ;  
     }  
     i f   ( t y p e o f   v a l u e   = = =   ' f u n c t i o n '   | |   v a l u e   i n s t a n c e o f   R e g E x p )   {  
         r e t u r n   v a l u e . t o S t r i n g ( ) ;  
     }  
     r e t u r n   v a l u e ;  
 }  
  
 f u n c t i o n   t r u n c a t e ( s ,   n )   {  
     i f   ( t y p e o f   s   = =   ' s t r i n g ' )   {  
         r e t u r n   s . l e n g t h   <   n   ?   s   :   s . s l i c e ( 0 ,   n ) ;  
     }   e l s e   {  
         r e t u r n   s ;  
     }  
 }  
  
 a s s e r t . A s s e r t i o n E r r o r . p r o t o t y p e . t o S t r i n g   =   f u n c t i o n ( )   {  
     i f   ( t h i s . m e s s a g e )   {  
         r e t u r n   [ t h i s . n a m e   +   ' : ' ,   t h i s . m e s s a g e ] . j o i n ( '   ' ) ;  
     }   e l s e   {  
         r e t u r n   [  
             t h i s . n a m e   +   ' : ' ,  
             t r u n c a t e ( J S O N . s t r i n g i f y ( t h i s . a c t u a l ,   r e p l a c e r ) ,   1 2 8 ) ,  
             t h i s . o p e r a t o r ,  
             t r u n c a t e ( J S O N . s t r i n g i f y ( t h i s . e x p e c t e d ,   r e p l a c e r ) ,   1 2 8 )  
         ] . j o i n ( '   ' ) ;  
     }  
 } ;  
  
 / /   A t   p r e s e n t   o n l y   t h e   t h r e e   k e y s   m e n t i o n e d   a b o v e   a r e   u s e d   a n d  
 / /   u n d e r s t o o d   b y   t h e   s p e c .   I m p l e m e n t a t i o n s   o r   s u b   m o d u l e s   c a n   p a s s  
 / /   o t h e r   k e y s   t o   t h e   A s s e r t i o n E r r o r ' s   c o n s t r u c t o r   -   t h e y   w i l l   b e  
 / /   i g n o r e d .  
  
 / /   3 .   A l l   o f   t h e   f o l l o w i n g   f u n c t i o n s   m u s t   t h r o w   a n   A s s e r t i o n E r r o r  
 / /   w h e n   a   c o r r e s p o n d i n g   c o n d i t i o n   i s   n o t   m e t ,   w i t h   a   m e s s a g e   t h a t  
 / /   m a y   b e   u n d e f i n e d   i f   n o t   p r o v i d e d .     A l l   a s s e r t i o n   m e t h o d s   p r o v i d e  
 / /   b o t h   t h e   a c t u a l   a n d   e x p e c t e d   v a l u e s   t o   t h e   a s s e r t i o n   e r r o r   f o r  
 / /   d i s p l a y   p u r p o s e s .  
  
 f u n c t i o n   f a i l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e ,   o p e r a t o r ,   s t a c k S t a r t F u n c t i o n )   {  
     t h r o w   n e w   a s s e r t . A s s e r t i o n E r r o r ( {  
         m e s s a g e :   m e s s a g e ,  
         a c t u a l :   a c t u a l ,  
         e x p e c t e d :   e x p e c t e d ,  
         o p e r a t o r :   o p e r a t o r ,  
         s t a c k S t a r t F u n c t i o n :   s t a c k S t a r t F u n c t i o n  
     } ) ;  
 }  
  
 / /   E X T E N S I O N !   a l l o w s   f o r   w e l l   b e h a v e d   e r r o r s   d e f i n e d   e l s e w h e r e .  
 a s s e r t . f a i l   =   f a i l ;  
  
 / /   4 .   P u r e   a s s e r t i o n   t e s t s   w h e t h e r   a   v a l u e   i s   t r u t h y ,   a s   d e t e r m i n e d  
 / /   b y   ! ! g u a r d .  
 / /   a s s e r t . o k ( g u a r d ,   m e s s a g e _ o p t ) ;  
 / /   T h i s   s t a t e m e n t   i s   e q u i v a l e n t   t o   a s s e r t . e q u a l ( t r u e ,   ! ! g u a r d ,  
 / /   m e s s a g e _ o p t ) ; .   T o   t e s t   s t r i c t l y   f o r   t h e   v a l u e   t r u e ,   u s e  
 / /   a s s e r t . s t r i c t E q u a l ( t r u e ,   g u a r d ,   m e s s a g e _ o p t ) ; .  
  
 f u n c t i o n   o k ( v a l u e ,   m e s s a g e )   {  
     i f   ( ! ! ! v a l u e )   f a i l ( v a l u e ,   t r u e ,   m e s s a g e ,   ' = = ' ,   a s s e r t . o k ) ;  
 }  
 a s s e r t . o k   =   o k ;  
  
 / /   5 .   T h e   e q u a l i t y   a s s e r t i o n   t e s t s   s h a l l o w ,   c o e r c i v e   e q u a l i t y   w i t h  
 / /   = = .  
 / /   a s s e r t . e q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e _ o p t ) ;  
  
 a s s e r t . e q u a l   =   f u n c t i o n   e q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e )   {  
     i f   ( a c t u a l   ! =   e x p e c t e d )   f a i l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e ,   ' = = ' ,   a s s e r t . e q u a l ) ;  
 } ;  
  
 / /   6 .   T h e   n o n - e q u a l i t y   a s s e r t i o n   t e s t s   f o r   w h e t h e r   t w o   o b j e c t s   a r e   n o t   e q u a l  
 / /   w i t h   ! =   a s s e r t . n o t E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e _ o p t ) ;  
  
 a s s e r t . n o t E q u a l   =   f u n c t i o n   n o t E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e )   {  
     i f   ( a c t u a l   = =   e x p e c t e d )   {  
         f a i l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e ,   ' ! = ' ,   a s s e r t . n o t E q u a l ) ;  
     }  
 } ;  
  
 / /   7 .   T h e   e q u i v a l e n c e   a s s e r t i o n   t e s t s   a   d e e p   e q u a l i t y   r e l a t i o n .  
 / /   a s s e r t . d e e p E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e _ o p t ) ;  
  
 a s s e r t . d e e p E q u a l   =   f u n c t i o n   d e e p E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e )   {  
     i f   ( ! _ d e e p E q u a l ( a c t u a l ,   e x p e c t e d ) )   {  
         f a i l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e ,   ' d e e p E q u a l ' ,   a s s e r t . d e e p E q u a l ) ;  
     }  
 } ;  
  
 f u n c t i o n   _ d e e p E q u a l ( a c t u a l ,   e x p e c t e d )   {  
     / /   7 . 1 .   A l l   i d e n t i c a l   v a l u e s   a r e   e q u i v a l e n t ,   a s   d e t e r m i n e d   b y   = = = .  
     i f   ( a c t u a l   = = =   e x p e c t e d )   {  
         r e t u r n   t r u e ;  
  
 / /     }   e l s e   i f   ( B u f f e r . i s B u f f e r ( a c t u a l )   & &   B u f f e r . i s B u f f e r ( e x p e c t e d ) )   {  
 / /         i f   ( a c t u a l . l e n g t h   ! =   e x p e c t e d . l e n g t h )   r e t u r n   f a l s e ;  
 / /  
 / /         f o r   ( v a r   i   =   0 ;   i   <   a c t u a l . l e n g t h ;   i + + )   {  
 / /             i f   ( a c t u a l [ i ]   ! = =   e x p e c t e d [ i ] )   r e t u r n   f a l s e ;  
 / /         }  
 / /  
 / /         r e t u r n   t r u e ;  
 / /  
     / /   7 . 2 .   I f   t h e   e x p e c t e d   v a l u e   i s   a   D a t e   o b j e c t ,   t h e   a c t u a l   v a l u e   i s  
     / /   e q u i v a l e n t   i f   i t   i s   a l s o   a   D a t e   o b j e c t   t h a t   r e f e r s   t o   t h e   s a m e   t i m e .  
     }   e l s e   i f   ( a c t u a l   i n s t a n c e o f   D a t e   & &   e x p e c t e d   i n s t a n c e o f   D a t e )   {  
         r e t u r n   a c t u a l . g e t T i m e ( )   = = =   e x p e c t e d . g e t T i m e ( ) ;  
  
     / /   7 . 3   I f   t h e   e x p e c t e d   v a l u e   i s   a   R e g E x p   o b j e c t ,   t h e   a c t u a l   v a l u e   i s  
     / /   e q u i v a l e n t   i f   i t   i s   a l s o   a   R e g E x p   o b j e c t   w i t h   t h e   s a m e   s o u r c e   a n d  
     / /   p r o p e r t i e s   ( ` g l o b a l ` ,   ` m u l t i l i n e ` ,   ` l a s t I n d e x ` ,   ` i g n o r e C a s e ` ) .  
     }   e l s e   i f   ( a c t u a l   i n s t a n c e o f   R e g E x p   & &   e x p e c t e d   i n s t a n c e o f   R e g E x p )   {  
         r e t u r n   a c t u a l . s o u r c e   = = =   e x p e c t e d . s o u r c e   & &  
                       a c t u a l . g l o b a l   = = =   e x p e c t e d . g l o b a l   & &  
                       a c t u a l . m u l t i l i n e   = = =   e x p e c t e d . m u l t i l i n e   & &  
                       a c t u a l . l a s t I n d e x   = = =   e x p e c t e d . l a s t I n d e x   & &  
                       a c t u a l . i g n o r e C a s e   = = =   e x p e c t e d . i g n o r e C a s e ;  
  
     / /   7 . 4 .   O t h e r   p a i r s   t h a t   d o   n o t   b o t h   p a s s   t y p e o f   v a l u e   = =   ' o b j e c t ' ,  
     / /   e q u i v a l e n c e   i s   d e t e r m i n e d   b y   = = .  
     }   e l s e   i f   ( t y p e o f   a c t u a l   ! =   ' o b j e c t '   & &   t y p e o f   e x p e c t e d   ! =   ' o b j e c t ' )   {  
         r e t u r n   a c t u a l   = =   e x p e c t e d ;  
  
     / /   7 . 5   F o r   a l l   o t h e r   O b j e c t   p a i r s ,   i n c l u d i n g   A r r a y   o b j e c t s ,   e q u i v a l e n c e   i s  
     / /   d e t e r m i n e d   b y   h a v i n g   t h e   s a m e   n u m b e r   o f   o w n e d   p r o p e r t i e s   ( a s   v e r i f i e d  
     / /   w i t h   O b j e c t . p r o t o t y p e . h a s O w n P r o p e r t y . c a l l ) ,   t h e   s a m e   s e t   o f   k e y s  
     / /   ( a l t h o u g h   n o t   n e c e s s a r i l y   t h e   s a m e   o r d e r ) ,   e q u i v a l e n t   v a l u e s   f o r   e v e r y  
     / /   c o r r e s p o n d i n g   k e y ,   a n d   a n   i d e n t i c a l   ' p r o t o t y p e '   p r o p e r t y .   N o t e :   t h i s  
     / /   a c c o u n t s   f o r   b o t h   n a m e d   a n d   i n d e x e d   p r o p e r t i e s   o n   A r r a y s .  
     }   e l s e   {  
         r e t u r n   o b j E q u i v ( a c t u a l ,   e x p e c t e d ) ;  
     }  
 }  
  
 f u n c t i o n   i s U n d e f i n e d O r N u l l ( v a l u e )   {  
     r e t u r n   v a l u e   = = =   n u l l   | |   v a l u e   = = =   u n d e f i n e d ;  
 }  
  
 f u n c t i o n   i s A r g u m e n t s ( o b j e c t )   {  
     r e t u r n   O b j e c t . p r o t o t y p e . t o S t r i n g . c a l l ( o b j e c t )   = =   ' [ o b j e c t   A r g u m e n t s ] ' ;  
 }  
  
 f u n c t i o n   o b j E q u i v ( a ,   b )   {  
     i f   ( i s U n d e f i n e d O r N u l l ( a )   | |   i s U n d e f i n e d O r N u l l ( b ) )  
         r e t u r n   f a l s e ;  
     / /   a n   i d e n t i c a l   ' p r o t o t y p e '   p r o p e r t y .  
     i f   ( a . p r o t o t y p e   ! = =   b . p r o t o t y p e )   r e t u r n   f a l s e ;  
     / / ~ ~ ~ I ' v e   m a n a g e d   t o   b r e a k   O b j e c t . k e y s   t h r o u g h   s c r e w y   a r g u m e n t s   p a s s i n g .  
     / /       C o n v e r t i n g   t o   a r r a y   s o l v e s   t h e   p r o b l e m .  
     i f   ( i s A r g u m e n t s ( a ) )   {  
         i f   ( ! i s A r g u m e n t s ( b ) )   {  
             r e t u r n   f a l s e ;  
         }  
         a   =   p S l i c e . c a l l ( a ) ;  
         b   =   p S l i c e . c a l l ( b ) ;  
         r e t u r n   _ d e e p E q u a l ( a ,   b ) ;  
     }  
     t r y   {  
         v a r   k a   =   O b j e c t . k e y s ( a ) ,  
                 k b   =   O b j e c t . k e y s ( b ) ,  
                 k e y ,   i ;  
     }   c a t c h   ( e )   { / / h a p p e n s   w h e n   o n e   i s   a   s t r i n g   l i t e r a l   a n d   t h e   o t h e r   i s n ' t  
         r e t u r n   f a l s e ;  
     }  
     / /   h a v i n g   t h e   s a m e   n u m b e r   o f   o w n e d   p r o p e r t i e s   ( k e y s   i n c o r p o r a t e s  
     / /   h a s O w n P r o p e r t y )  
     i f   ( k a . l e n g t h   ! =   k b . l e n g t h )  
         r e t u r n   f a l s e ;  
     / / t h e   s a m e   s e t   o f   k e y s   ( a l t h o u g h   n o t   n e c e s s a r i l y   t h e   s a m e   o r d e r ) ,  
     k a . s o r t ( ) ;  
     k b . s o r t ( ) ;  
     / / ~ ~ ~ c h e a p   k e y   t e s t  
     f o r   ( i   =   k a . l e n g t h   -   1 ;   i   > =   0 ;   i - - )   {  
         i f   ( k a [ i ]   ! =   k b [ i ] )  
             r e t u r n   f a l s e ;  
     }  
     / / e q u i v a l e n t   v a l u e s   f o r   e v e r y   c o r r e s p o n d i n g   k e y ,   a n d  
     / / ~ ~ ~ p o s s i b l y   e x p e n s i v e   d e e p   t e s t  
     f o r   ( i   =   k a . l e n g t h   -   1 ;   i   > =   0 ;   i - - )   {  
         k e y   =   k a [ i ] ;  
         i f   ( ! _ d e e p E q u a l ( a [ k e y ] ,   b [ k e y ] ) )   r e t u r n   f a l s e ;  
     }  
     r e t u r n   t r u e ;  
 }  
  
 / /   8 .   T h e   n o n - e q u i v a l e n c e   a s s e r t i o n   t e s t s   f o r   a n y   d e e p   i n e q u a l i t y .  
 / /   a s s e r t . n o t D e e p E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e _ o p t ) ;  
  
 a s s e r t . n o t D e e p E q u a l   =   f u n c t i o n   n o t D e e p E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e )   {  
     i f   ( _ d e e p E q u a l ( a c t u a l ,   e x p e c t e d ) )   {  
         f a i l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e ,   ' n o t D e e p E q u a l ' ,   a s s e r t . n o t D e e p E q u a l ) ;  
     }  
 } ;  
  
 / /   9 .   T h e   s t r i c t   e q u a l i t y   a s s e r t i o n   t e s t s   s t r i c t   e q u a l i t y ,   a s   d e t e r m i n e d   b y   = = = .  
 / /   a s s e r t . s t r i c t E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e _ o p t ) ;  
  
 a s s e r t . s t r i c t E q u a l   =   f u n c t i o n   s t r i c t E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e )   {  
     i f   ( a c t u a l   ! = =   e x p e c t e d )   {  
         f a i l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e ,   ' = = = ' ,   a s s e r t . s t r i c t E q u a l ) ;  
     }  
 } ;  
  
 / /   1 0 .   T h e   s t r i c t   n o n - e q u a l i t y   a s s e r t i o n   t e s t s   f o r   s t r i c t   i n e q u a l i t y ,   a s  
 / /   d e t e r m i n e d   b y   ! = = .     a s s e r t . n o t S t r i c t E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e _ o p t ) ;  
  
 a s s e r t . n o t S t r i c t E q u a l   =   f u n c t i o n   n o t S t r i c t E q u a l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e )   {  
     i f   ( a c t u a l   = = =   e x p e c t e d )   {  
         f a i l ( a c t u a l ,   e x p e c t e d ,   m e s s a g e ,   ' ! = = ' ,   a s s e r t . n o t S t r i c t E q u a l ) ;  
     }  
 } ;  
  
 f u n c t i o n   e x p e c t e d E x c e p t i o n ( a c t u a l ,   e x p e c t e d )   {  
     i f   ( ! a c t u a l   | |   ! e x p e c t e d )   {  
         r e t u r n   f a l s e ;  
     }  
  
     i f   ( e x p e c t e d   i n s t a n c e o f   R e g E x p )   {  
         r e t u r n   e x p e c t e d . t e s t ( a c t u a l ) ;  
     }   e l s e   i f   ( a c t u a l   i n s t a n c e o f   e x p e c t e d )   {  
         r e t u r n   t r u e ;  
     }   e l s e   i f   ( e x p e c t e d . c a l l ( { } ,   a c t u a l )   = = =   t r u e )   {  
         r e t u r n   t r u e ;  
     }  
  
     r e t u r n   f a l s e ;  
 }  
  
 f u n c t i o n   _ t h r o w s ( s h o u l d T h r o w ,   b l o c k ,   e x p e c t e d ,   m e s s a g e )   {  
     v a r   a c t u a l ;  
  
     i f   ( t y p e o f   e x p e c t e d   = = =   ' s t r i n g ' )   {  
         m e s s a g e   =   e x p e c t e d ;  
         e x p e c t e d   =   n u l l ;  
     }  
  
     t r y   {  
         b l o c k ( ) ;  
     }   c a t c h   ( e )   {  
         a c t u a l   =   e ;  
     }  
  
     m e s s a g e   =   ( e x p e c t e d   & &   e x p e c t e d . n a m e   ?   '   ( '   +   e x p e c t e d . n a m e   +   ' ) . '   :   ' . ' )   +  
                         ( m e s s a g e   ?   '   '   +   m e s s a g e   :   ' . ' ) ;  
  
     i f   ( s h o u l d T h r o w   & &   ! a c t u a l )   {  
         f a i l ( a c t u a l ,   e x p e c t e d ,   ' M i s s i n g   e x p e c t e d   e x c e p t i o n '   +   m e s s a g e ) ;  
     }  
  
     i f   ( ! s h o u l d T h r o w   & &   e x p e c t e d E x c e p t i o n ( a c t u a l ,   e x p e c t e d ) )   {  
         f a i l ( a c t u a l ,   e x p e c t e d ,   ' G o t   u n w a n t e d   e x c e p t i o n '   +   m e s s a g e ) ;  
     }  
  
     i f   ( ( s h o u l d T h r o w   & &   a c t u a l   & &   e x p e c t e d   & &  
             ! e x p e c t e d E x c e p t i o n ( a c t u a l ,   e x p e c t e d ) )   | |   ( ! s h o u l d T h r o w   & &   a c t u a l ) )   {  
         t h r o w   a c t u a l ;  
     }  
 }  
  
 / /   1 1 .   E x p e c t e d   t o   t h r o w   a n   e r r o r :  
 / /   a s s e r t . t h r o w s ( b l o c k ,   E r r o r _ o p t ,   m e s s a g e _ o p t ) ;  
  
 a s s e r t . t h r o w s   =   f u n c t i o n ( b l o c k ,   / * o p t i o n a l * / e r r o r ,   / * o p t i o n a l * / m e s s a g e )   {  
     _ t h r o w s . a p p l y ( t h i s ,   [ t r u e ] . c o n c a t ( p S l i c e . c a l l ( a r g u m e n t s ) ) ) ;  
 } ;  
  
 / /   E X T E N S I O N !   T h i s   i s   a n n o y i n g   t o   w r i t e   o u t s i d e   t h i s   m o d u l e .  
 a s s e r t . d o e s N o t T h r o w   =   f u n c t i o n ( b l o c k ,   / * o p t i o n a l * / m e s s a g e )   {  
     _ t h r o w s . a p p l y ( t h i s ,   [ f a l s e ] . c o n c a t ( p S l i c e . c a l l ( a r g u m e n t s ) ) ) ;  
 } ;  
  
 a s s e r t . i f E r r o r   =   f u n c t i o n ( e r r )   {   i f   ( e r r )   { t h r o w   e r r ; } } ;  
  
 i f   ( t y p e o f   d e f i n e   = = =   ' f u n c t i o n '   & &   d e f i n e . a m d )   {  
     d e f i n e ( ' a s s e r t ' ,   f u n c t i o n   ( )   {  
         r e t u r n   a s s e r t ;  
     } ) ;  
 }  
  
 } ) ( t h i s ) ;  
  
 
});