define('fs/index', ["fs/indexeddb","fs/directory_entry"], function(require, exports, module) {
exports = module.exports = require('fs/indexeddb');

exports.DirectoryEntry = require('fs/directory_entry');

exports.DirectoryEntry.prototype.readFile = function (callback) {
  if (this.type !== 'file') {
    throw new TypeError('Not a file.');
  }
  return exports.readFile(this.path, callback);
};

});