define('fs/directory_entry', ["path"], function(require, exports, module) {
var path = require('path');

function DirectoryEntry(fullPath, type) {
  this.path = fullPath;
  this.name = path.basename(fullPath);
  this.dir = path.dirname(fullPath);
  this.type = type;
}

module.exports = DirectoryEntry;
});