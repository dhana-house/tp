define('pickadate/v2-(deprecated)/pickadate.js/translations/pickadate.eu_ES', [], function(require, exports, module) {
// Basque

$.extend( $.fn.pickadate.defaults, {
    monthsFull: [ 'urtarrila', 'otsaila', 'martxoa', 'apirila', 'maiatza', 'ekaina', 'uztaila', 'abuztua', 'iraila', 'urria', 'azaroa', 'abendua' ],
    monthsShort: [ 'urt', 'ots', 'mar', 'api', 'mai', 'eka', 'uzt', 'abu', 'ira', 'urr', 'aza', 'abe' ],
    weekdaysFull: [ 'igandea', 'astelehena', 'asteartea', 'asteazkena', 'osteguna', 'ostirala', 'larunbata' ],
    weekdaysShort: [ 'ig.', 'al.', 'ar.', 'az.', 'og.', 'or.', 'lr.' ],
    today: 'gaur',
    clear: 'garbitu',
    firstDay: 1,
    format: 'dddd, yyyy(e)ko mmmmren da',
    formatSubmit: 'yyyy/mm/dd'
})

});