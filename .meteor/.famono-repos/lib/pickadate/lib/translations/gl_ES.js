define('pickadate/lib/translations/gl_ES', [], function(require, exports, module) {
// Galician

jQuery.extend( jQuery.fn.pickadate.defaults, {
    monthsFull: [ 'Xaneiro', 'Febreiro', 'Marzo', 'Abril', 'Maio', 'Xuño', 'Xullo', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Decembro' ],
    monthsShort: [ 'xan', 'feb', 'mar', 'abr', 'mai', 'xun', 'xul', 'ago', 'sep', 'out', 'nov', 'dec' ],
    weekdaysFull: [ 'domingo', 'luns', 'martes', 'mércores', 'xoves', 'venres', 'sábado' ],
    weekdaysShort: [ 'dom', 'lun', 'mar', 'mér', 'xov', 'ven', 'sab' ],
    today: 'hoxe',
    clear: 'borrar',
    firstDay: 1,
    format: 'dddd d !de mmmm !de yyyy',
    formatSubmit: 'yyyy/mm/dd'
});

});