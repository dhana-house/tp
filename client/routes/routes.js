Router.map(function () {
  this.route('userHome', {
    path: '/',
    template: 'userHome',
    layoutTemplate: 'user'
  });

  this.route('userFilterSleep', {
		path: '/filter/sleep',
		template: 'userFilterSleep',
		layoutTemplate: 'user'
  });

});