Meteor.startup(function () {
  require("famous-polyfills");
  require("famous/core/famous");
  require("famous/core/Engine");
  require("famous/core/Modifier");
  require("famous/core/Surface");
  require("famous/views/SequentialLayout");
  require("famous/modifiers/StateModifier");
  require("famous/views/Scrollview");
  require("famous/core/View");
});
